﻿using AutoMapper;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Controllers.DTOs.Supplier;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Dtos;
using SeanTIME_bend.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Mapping
{
    public class MappingProfile : Profile
    {
        // Domain to API Resource
        public MappingProfile() {
            CreateMap(typeof(QueryResult<>), typeof(QueryResultDTO<>));
            CreateMap<ContactDTO, Contact>();
            CreateMap<SupplierDTO, Supplier>();
            CreateMap<SupplierQueryDTO, SupplierQuery>();
            CreateMap<ContactQueryDTO, ContactQuery>();
            CreateMap<ClientQueryDTO, ClientQuery>();
            CreateMap<AddressQueryDTO, AddressQuery>();
            CreateMap<JobQueryDTO, JobQuery>();
            CreateMap<PreProductionDTO, JobQuery>();
            CreateMap<UserQueryDTO, UserQuery>();
            CreateMap<SaveContactDTO, Contact>()
                .ForMember(c => c.Id, opt => opt.Ignore());
            CreateMap<SaveClientDTO, Client>();
            CreateMap<SaveJobDTO, Job>();
            CreateMap<SaveQuoteDTO, Job>();
            CreateMap<SavePreProductionDTO, Job>();
            CreateMap<SaveSupplierDTO, Supplier>();
            CreateMap<SaveAddressDTO, Address>();

            //API to domain
            //Creates client by removing jobs and contact
            CreateMap<Client, ClientDTO>();
            CreateMap<Contact, ContactDTO>();
            CreateMap<Job, JobDTO>();
            CreateMap<Job, PreProductionDTO>();
            CreateMap<Supplier, SupplierDTO>();
            CreateMap<User, UserDTO>()
                .ForMember(u => u.Password, opt => opt.Ignore());
            CreateMap<Address, AddressDTO>();

        }
    }
}
