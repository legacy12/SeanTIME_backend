﻿using SeanTIME_bend.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Persistance;
using SeanTIME_bend.Helpers;
using SeanTIME_bend.Extensions;
using Microsoft.EntityFrameworkCore;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace SeanTIME_bend.Services {
    public class JobService : IJobService {

        private SeanTIMEDbContext _context;

        public JobService(SeanTIMEDbContext context) {
            _context = context;
        }

        public Job Create(Job job) {

            // Adds suppliers to context since they're already there
            if(job.PreItems != null)
            {
                for(int i = 0; i < job.PreItems.Count; i ++)
                {
                    var preItem = job.PreItems.ElementAt(i);

                    for(int j = 0; j < preItem.SupplierContactItems.Count; j++)
                    {
                        _context.Supplier.Attach(preItem.SupplierContactItems.ElementAt(j).Supplier);
                    }
                }
            }

            _context.Job.Add(job);
            _context.SaveChanges();

            return job;
        }

        public void Delete(Guid id) {
            var job = _context.Job.Find(id);
            if (job != null) {
                _context.Job.Remove(job);
                _context.SaveChanges();
            }
        }

        public QueryResult<Job> GetJobs(JobQuery queryObj) {
            var result = new QueryResult<Job>();

            var query = _context.Job
                .Include(j => j.Contact)
                .Include(j => j.Client)
                .Include(j => j.User)
                .Include(j => j.Milestones)
                .Where(j => j.JobState == JobState.Production)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public QueryResult<Job> GetByClient(JobQuery queryObj) {
            var result = new QueryResult<Job>();

            var query = _context.Job
                .AsQueryable()
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;

        }

        public Job GetById(Guid id) {
            return _context.Job
                .Include(j => j.Contact)
                .Include(j => j.Client)
                .Include(j => j.User)
                .Include(j => j.Notes)
                .Include(j => j.Items)
                    .ThenInclude(i => i.Supplier)
                .Include(j => j.Milestones)
                .Include(j => j.PreItems)
                .AsNoTracking() //This is needed or else notes complain about already being in the context
                .SingleOrDefault(x => x.Id == id);
        }

        public QueryResult<Job> GetById(Guid id, JobQuery queryObj) {
            var result = new QueryResult<Job>();

            var query = _context.Job
                .Where(j => j.Id == id)
                .Include(j => j.Contact)
                .Include(j => j.Client)
                .Include(j => j.User)
                .Include(j => j.Notes)
                .Include(j => j.Items)
                    .ThenInclude(i => i.Supplier)
                .Include(j => j.Milestones)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public QueryResult<Job> GetPreProductionById(Guid id, JobQuery queryObj)
        {
            var result = new QueryResult<Job>();

            var query = _context.Job
                .Where(j => j.Id == id)
                .Include(j => j.Contact)
                .Include(j => j.Client)
                .Include(j => j.User)
                .Include(j => j.Notes)
                .Include(j => j.Items)
                    .ThenInclude(i => i.Supplier)
                .Include(j => j.Milestones)
                .Include(j => j.PreItems)
                    .ThenInclude(p => p.SupplierContactItems)
                        .ThenInclude(s => s.Supplier)
                .Include(j => j.PreItems)
                    .ThenInclude(p => p.SupplierContactItems)
                        .ThenInclude(s => s.Contact)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public void Update(Job job, Job jobParam) {
            if (job == null) {
                throw new AppException("Job does not exist");
            }

            //Update job properties
            job.Description = jobParam.Description;
            job.Notes = jobParam.Notes;
            job.Title = jobParam.Title;

            for (int i=0; i < job.Items.Count; i++) {
                
                if(jobParam.Items.Contains(job.Items.ElementAt(i))) {
                    // job keeps item
                } else {
                    _context.Item.Remove(job.Items.ElementAt(i));
                }
            }

            _context.Job.Update(job);
            _context.SaveChanges();

            // This seems ridiculous but the context loads items when searching for delete
            // so we need to do this and send to database with two different updates

            job.Items = jobParam.Items;

            _context.Job.Update(job);
            _context.SaveChanges();
        }

        public QueryResult<Job> GetQuotes(JobQuery queryObj) {
            var result = new QueryResult<Job>();

            var query = _context.Job
                .Include(j => j.Contact)
                .Include(j => j.Client)
                .Include(j => j.User)
                .Include(j => j.Milestones)
                .Where(j => j.JobState == JobState.Quote)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public QueryResult<Job> GetInvoicing(JobQuery queryObj) {
            var result = new QueryResult<Job>();

            var query = _context.Job
                .Include(j => j.Contact)
                .Include(j => j.Client)
                .Include(j => j.User)
                .Include(j => j.Milestones)
                .Where(j => j.JobState == JobState.Invoicing)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public QueryResult<Job> GetCompleted(JobQuery queryObj) {
            var result = new QueryResult<Job>();

            var query = _context.Job
                .Include(j => j.Contact)
                .Include(j => j.Client)
                .Include(j => j.User)
                .Include(j => j.Milestones)
                .Where(j => j.JobState == JobState.Completed)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public void UpgradePreProduction(Job jobParam)
        {
            if (jobParam == null)
            {
                throw new AppException("Job does not exist");
            }
            var job = GetById(jobParam.Id);

            job.Milestones = jobParam.Milestones;

            job.JobState = JobState.Quote;

            _context.Job.Update(job);
            _context.SaveChanges();
        }

        //Updating quotes was causing issues - https://stackoverflow.com/questions/48665180/auto-increment-non-key-value-entity-framework-core-2-0
        public void UpgradeQuote(Job jobParam) {
            if (jobParam == null) {
                throw new AppException("Job does not exist");
            }
            var job = GetById(jobParam.Id);

            job.Milestones = jobParam.Milestones;

            job.JobState = JobState.Production;

            _context.Job.Update(job);
            _context.SaveChanges();
        }

        public void UpgradeProduction(Job jobParam) {
            if (jobParam == null) {
                throw new AppException("Job does not exist");
            }
            var job = GetById(jobParam.Id);

            job.Milestones = jobParam.Milestones;
            job.JobState = JobState.Invoicing;

            _context.Job.Update(job);
            _context.SaveChanges();
        }

        public void UpgradeInvoicing(Job jobParam) {
            if (jobParam == null) {
                throw new AppException("Job does not exist");
            }
            var job = GetById(jobParam.Id);

            job.Milestones = jobParam.Milestones;
            job.JobState = JobState.Completed;

            _context.Job.Update(job);
            _context.SaveChanges();
        }
    }
}
