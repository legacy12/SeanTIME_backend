﻿using SeanTIME_bend.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Persistance;
using SeanTIME_bend.Helpers;
using SeanTIME_bend.DTOs;
using Microsoft.EntityFrameworkCore;
using SeanTIME_bend.Extensions;

namespace SeanTIME_bend.Services
{
    public class ClientService : IClientService {

        private SeanTIMEDbContext _context;

        public ClientService(SeanTIMEDbContext context) {
            _context = context;
        }

        public Client Create(Client client) {

            _context.Contact.Attach(client.Contact);
            // validation
            if (_context.Client.Any(x => x.Name == client.Name))
                throw new AppException("Client " + client.Name + " is already taken");

            _context.Client.Add(client);
            _context.SaveChanges();

            return client;
        }

        public void Delete(Guid id) {
            var client = _context.Client.Find(id);
            if (client != null) {
                _context.Client.Remove(client);
                _context.SaveChanges();
            }
        }

        public QueryResult<Client> GetClients(ClientQuery queryObj) {
            var result = new QueryResult<Client>();

            var query = _context.Client
                .Include(c => c.Contact)
                .Include(c => c.Jobs)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public Client GetById(Guid Id) {
            var query = _context.Client
                .Include(c => c.Contact)
                .Include(c => c.Jobs)
                    .ThenInclude(j => j.Contact)
                .Include(c => c.Jobs)
                    .ThenInclude(j => j.User)
                .Include(c => c.Jobs)
                .Include(c => c.Addresses)
                .Include(c => c.User)
                .Where(c => c.Id == Id)
                .AsQueryable()
                .AsNoTracking();
                

            return query.FirstOrDefault();
        }

        public void Update(Client clientParam) {
            var client = GetById(clientParam.Id);

            _context.Contact.Attach(clientParam.Contact);

            if (client == null)
                throw new AppException("Client not found");

            // update client properties
            client.Name = clientParam.Name;
            client.Contact = clientParam.Contact;
            client.Description = clientParam.Description;
            client.Jobs = clientParam.Jobs;
            client.OnStop = clientParam.OnStop;

            _context.Client.Update(client);
            _context.SaveChanges();
        }

        // TODO Currently no way to delete an address
        public Client UpdateAddresses(Client client, Client clientParam)
        {
            for (int i = 0; i < clientParam.Addresses.Count; i++)
            {
                if (client.Addresses.Contains(clientParam.Addresses.ElementAt(i))) {
                    // Address that has been updated
                    _context.Entry(client.Addresses.ElementAt(i)).CurrentValues.SetValues(clientParam.Addresses.ElementAt(i));
                } 
                else
                {
                    // Adding new address
                    client.Addresses.Add(clientParam.Addresses.ElementAt(i));
                }
            }

            return client;
        }

        public void UpdateOnStop(Guid id, bool onStop) {
            var client = _context.Client.Find(id);

            if (client == null)
                throw new AppException("Client not found");

            client.OnStop = onStop;

            _context.Client.Update(client);
            _context.SaveChanges();
        }
    }
}
