﻿using Microsoft.EntityFrameworkCore;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Extensions;
using SeanTIME_bend.Helpers;
using SeanTIME_bend.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Services
{
    public class SupplierService : ISupplierService {

        private SeanTIMEDbContext _context;

        public SupplierService(SeanTIMEDbContext context) {
            _context = context;
        }

        public Supplier Create(Supplier supplier) {
            
            // Contacts already created so only need to add the reference in supplier table
            for(int i=0; i<supplier.Contacts.Count; i++) {
                _context.Contact.Attach(supplier.Contacts.ElementAt(i));
            }

            _context.Supplier.Add(supplier);
            _context.SaveChanges();
            return supplier;
        }

        public void Delete(Guid id) {
            var supplier = _context.Supplier.Find(id);
            if (supplier != null) {
                _context.Supplier.Remove(supplier);
                _context.SaveChanges();
            }
        }

        public QueryResult<Supplier> GetSuppliers(SupplierQuery queryObj) {
            var result = new QueryResult<Supplier>();

            var query = _context.Supplier
                .Include(s => s.Contacts)
                .Include(s => s.Items)
                .Include(s => s.Issues)
                .AsQueryable();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public Supplier GetById(Guid id) {
            return _context.Supplier
                .Include(s => s.Contacts)
                .Include(s => s.Issues)
                .AsNoTracking() //This is needed or else notes complain about already being in the context
                .SingleOrDefault(x => x.Id == id);
        }

        public Boolean GetSupplierFromContact(Contact contact) {
            return false;
        }

        public void Update(Supplier supplierParam) {
            var supplier = _context.Supplier.Find(supplierParam.Id);

            if (supplier == null)
                throw new AppException("Supplier not found");

            // update supplier properties
            supplier.Name = supplierParam.Name;
            supplier.Items = supplierParam.Items;
            supplier.PhoneNumber = supplierParam.PhoneNumber;
            supplier.Issues = supplierParam.Issues;

            for (int i = 0; i < supplier.Issues.Count; i++) {

                if (supplierParam.Issues.Contains(supplier.Issues.ElementAt(i))) {
                    // job keeps item
                }
                else {
                    _context.Issues.Remove(supplier.Issues.ElementAt(i));
                }
            }

            _context.Supplier.Update(supplier);
            _context.SaveChanges();
        }
    }
}
