﻿using SeanTIME_bend.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Persistance;
using SeanTIME_bend.Helpers;
using Microsoft.EntityFrameworkCore;
using SeanTIME_bend.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace SeanTIME_bend.Services
{
    public class ContactService : IContactService {

        private SeanTIMEDbContext _context;

        public ContactService(SeanTIMEDbContext context) {
            _context = context;
        }

        public Contact Create(Contact contact) {
            _context.Contact.Add(contact);
            _context.SaveChanges();
            return contact;
        }

        public void Delete(Guid id) {
            var contact = _context.Contact.Find(id);
            if (contact != null) {
                _context.Contact.Remove(contact);
                _context.SaveChanges();
            }
        }

        public QueryResult<Contact> GetContacts(ContactQuery queryObj, ContactRole role) {
            var result = new QueryResult<Contact>();

            var query = _context.Contact
                .Include(c => c.Jobs)
                .Where(c => c.ContactRole == role)
                .AsQueryable();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public QueryResult<Contact> GetContacts(ContactQuery queryObj) {
            var result = new QueryResult<Contact>();

            var query = _context.Contact
                .Include(c => c.Jobs)
                .AsQueryable();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public Contact GetById(Guid id) {
            return _context.Contact
                .Include(c => c.Jobs)
                .Where(c => c.Id == id)
                .SingleOrDefault();
        }

        public void Update(Guid id, Contact contactParam) {
            var contact = _context.Contact.Find(id);
            if (contact == null) {
                throw new AppException("Contact does not exist");
            }

            contact.ContactRole = contactParam.ContactRole;
            contact.Name = contactParam.Name;
            contact.PhoneNumber = contactParam.PhoneNumber;
            contact.Email = contactParam.Email;
            contact.Jobs = contactParam.Jobs;

            _context.Contact.Update(contact);
            _context.SaveChanges();
        }

        public QueryResult<Contact> GetUnassignedContacts(ContactQuery queryObj) {
            var result = new QueryResult<Contact>();

            var query = _context.Contact
                .Include(c => c.Supplier)
                .Where(c => c.Supplier == null)
                .AsQueryable();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public QueryResult<Contact> GetUnassignedContacts(ContactQuery queryObj, ContactRole role) {
            var result = new QueryResult<Contact>();

            var query = _context.Contact
                .Include(c => c.Supplier)
                .Where(c => c.Supplier == null)
                .Where(c => c.ContactRole == role)
                .AsQueryable();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }
    }
}
