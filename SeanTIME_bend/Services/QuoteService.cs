﻿using SeanTIME_bend.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Persistance;
using SeanTIME_bend.Helpers;
using Microsoft.EntityFrameworkCore;
using SeanTIME_bend.Extensions;

namespace SeanTIME_bend.Services
{
    public class QuoteService : IQuoteService {

        private SeanTIMEDbContext _context;

        public QuoteService(SeanTIMEDbContext context) {
            _context = context;
        }

        public void ChangeToJob(Guid id) {
            throw new NotImplementedException();
        }

        public Quote Create(Quote quote) {

            _context.Quote.Add(quote);
            _context.SaveChanges();

            return quote;
        }

        public void Delete(Guid id) {
            var quote = _context.Quote.Find(id);
            if (quote != null) {
                _context.Quote.Remove(quote);
                _context.SaveChanges();
            }
        }

        public Quote GetById(Guid id) {
            return _context.Quote
                .Include(j => j.Contact)
                .Include(j => j.Client)
                .Include(j => j.User)
                .Include(j => j.Notes)
                .Include(j => j.Items)
                .AsNoTracking() //This is needed or else notes complain about already being in the context
                .SingleOrDefault(x => x.Id == id);
        }

        public QueryResult<Quote> GetById(Guid id, QuoteQuery queryObj) {
            var result = new QueryResult<Quote>();

            var query = _context.Quote
                .Where(j => j.Id == id)
                .Include(j => j.Contact)
                .Include(j => j.Client)
                .Include(j => j.User)
                .Include(j => j.Notes)
                .Include(j => j.Items)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public QueryResult<Quote> GetQuotes(QuoteQuery filter) {
            var result = new QueryResult<Quote>();

            var query = _context.Quote
                .Include(q => q.Contact)
                .Include(q => q.Client)
                .Include(q => q.User)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(filter);
            result.Items = query.ToList();
            return result;
        }

        public void Update(Quote quote, Quote quoteParam) {
            if (quote == null) {
                throw new AppException("Job does not exist");
            }

            //Update job properties
            quote.Description = quoteParam.Description;
            quote.Markup = quoteParam.Markup;
            quote.Notes = quoteParam.Notes;
            quote.PurchasePrice = quoteParam.PurchasePrice;
            quote.SellingPrice = quoteParam.SellingPrice;
            quote.Title = quoteParam.Title;

            _context.Quote.Update(quote);
            _context.SaveChanges();
        }
    }
}
