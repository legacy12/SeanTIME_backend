﻿using SeanTIME_bend.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeanTIME_bend.Persistance;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Helpers;
using Microsoft.EntityFrameworkCore;
using SeanTIME_bend.Extensions;

namespace SeanTIME_bend.Services
{
    public class UserService : IUserService {

        private SeanTIMEDbContext _context;

        public UserService(SeanTIMEDbContext context) {
            _context = context;
        }

        public User Authenticate(string username, string password) {

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            //TODO Need to change this to have actual username
            var user = _context.User.SingleOrDefault(x => x.Email == username);

            //Check if user member exists
            if(user == null) {
                return null;
            }

            //Verify password
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            return user;
        }

        public IEnumerable<User> GetAll() {
            return _context.User;
        }

        public User GetById(Guid id) {
            return _context.User.Find(id);
        }

        public User Create(User user, string password) {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (_context.User.Any(x => x.Email == user.Email))
                throw new AppException("Email " + user.Email + " is already taken");

            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _context.User.Add(user);
            _context.SaveChanges();

            return user;
        }

        public void Update(User userParam, string password = null) {
            var user = _context.User.Find(userParam.Id);

            if (user == null)
                throw new AppException("User not found");

            if (userParam.Email != user.Email) {
                // username has changed so check if the new username is already taken
                if (_context.User.Any(x => x.Email == userParam.Email))
                    throw new AppException("Username " + userParam.Email + " is already taken");
            }

            // update user properties
            user.Name = userParam.Name;
            user.Email = userParam.Email;

            // update password if it was entered
            if (!string.IsNullOrWhiteSpace(password)) {
                CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            _context.User.Update(user);
            _context.SaveChanges();
        }

        public void Delete(Guid id) {
            var user = _context.User.Find(id);
            if (user != null) {
                _context.User.Remove(user);
                _context.SaveChanges();
            }
        }

        //Private helper methods
        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt) {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512()) {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt) {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt)) {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++) {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        public QueryResult<User> GetUsers(UserQuery queryObj) {
            var result = new QueryResult<User>();

            var query = _context.User
                .Include(c => c.Jobs)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }
    }
}
