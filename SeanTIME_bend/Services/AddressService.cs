﻿using Microsoft.EntityFrameworkCore;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Extensions;
using SeanTIME_bend.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Services
{
    public class AddressService : IAddressService
    {

        private SeanTIMEDbContext _context;

        public AddressService(SeanTIMEDbContext context)
        {
            _context = context;
        }

        public QueryResult<Address> GetAddresses(AddressQuery queryObj)
        {
            var result = new QueryResult<Address>();

            var query = _context.Addresses
                .Where(a => a.ClientId == queryObj.ClientId)
                .AsNoTracking();

            result.TotalItems = query.Count();
            query = query.ApplyPaging(queryObj);
            result.Items = query.ToList();
            return result;
        }

        public void AddAddress(Address address)
        {
            _context.Addresses.Add(address);
            _context.SaveChanges();
        }

        public void DeleteAddress(Guid id)
        {
            var address = _context.Addresses.Find(id);
            if (address != null)
            {
                _context.Addresses.Remove(address);
                _context.SaveChanges();
            }

        }

        public void EditAddress(Address address)
        {
            throw new NotImplementedException();
        }
    }
}
