﻿using System;
//using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using SeanTIME_bend.Helpers;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Dtos;

namespace SeanTIME_bend.Controllers {

    [Route("api/[controller]")]
    public class TokenAuthController : Controller {

        private IUserService _userService;

        public TokenAuthController(IUserService userService) {
            _userService = userService;
        }
        // https://www.pointblankdevelopment.com.au/blog/113/aspnet-core-angular-24-user-registration-and-login-tutorial-example#users-controller-cs
        // https://github.com/Longfld/ASPNETcoreAngularJWT
        [HttpPut("Login")]
        public IActionResult Login([FromBody]UserDTO userDTO) {

            var user = _userService.Authenticate(userDTO.Email, userDTO.Password);

            if (user != null) {

                var requestAt = DateTime.Now;
                var expiresIn = requestAt + TokenAuthOption.ExpiresSpan;
                var token = GenerateToken(user, expiresIn);

                // Removes passwords from user item before passsing back
                userDTO.Id = user.Id;
                userDTO.Name = user.Name;
                userDTO.Role = user.Role;
                userDTO.Password = "";


                return Json(new RequestResult {
                    State = RequestState.Success,
                    Data = new {
                        User = userDTO,
                        requertAt = requestAt,
                        expiresIn = TokenAuthOption.ExpiresSpan.TotalSeconds,
                        tokeyType = TokenAuthOption.TokenType,
                        accessToken = token
                    }
                });
            }
            else {
                return Json(new RequestResult {
                    State = RequestState.Failed,
                    Msg = "Username or password is invalid"
                });
            }
        }

        private string GenerateToken(User user, DateTime expires) {
            var handler = new JwtSecurityTokenHandler();

            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(user.Email, "TokenAuth"),
                new[] { new Claim("ID", user.Id.ToString()) }
            );

            var securityToken = handler.CreateToken(new SecurityTokenDescriptor {
                Issuer = TokenAuthOption.Issuer,
                Audience = TokenAuthOption.Audience,
                SigningCredentials = TokenAuthOption.SigningCredentials,
                Subject = identity,
                Expires = expires
            });
            return handler.WriteToken(securityToken);
        }

        // Authorization Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InVzZXIxIiwiSUQiOiJmNzdlOWQ4

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        public IActionResult GetUserInfo() {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            return Json(new RequestResult {
                State = RequestState.Success,
                Data = new { UserName = claimsIdentity.Name }
            });
        }

        [HttpGet("newAdmin")]
        public IActionResult NewAdmin() {
            string password = "seanTIME";
            User user = new User {
                Email = "seanTIME@seanTIME.com",
                Role = Role.Admin,
                Name = "seanTIME",
                Id = Guid.NewGuid()
            };
            _userService.Create(user, password);
            return Ok(user);
        }
    }
}

