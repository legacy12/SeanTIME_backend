﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService,
                        IMapper mapper) {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet]
        public QueryResultDTO<UserDTO> GetUsers(UserQueryDTO filterResource) {
            var filter = _mapper.Map<UserQueryDTO, UserQuery>(filterResource);
            var queryResult = _userService.GetUsers(filter);

            return _mapper.Map<QueryResult<User>, QueryResultDTO<UserDTO>>(queryResult);
        }

    }
}
