﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Controllers.DTOs.Supplier;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers
{
    [Route("api/[controller]")]
    public class SupplierController : Controller
    {
        private readonly ISupplierService _supplierService;
        private readonly IMapper _mapper;

        public SupplierController(ISupplierService supplierService,
                                  IMapper mapper) {
            _supplierService = supplierService;
            _mapper = mapper;
        }

        [HttpGet]
        public QueryResultDTO<SupplierDTO> GetSuppliers(SupplierQueryDTO filterResource) {
            var filter = _mapper.Map<SupplierQueryDTO, SupplierQuery>(filterResource);
            var queryResult = _supplierService.GetSuppliers(filter);
            Console.WriteLine("suppliers returned: " + queryResult.Items.ToString());
            return _mapper.Map<QueryResult<Supplier>, QueryResultDTO<SupplierDTO>>(queryResult);
        }

        [HttpGet("{id}")]
        public IActionResult GetSupplier(Guid id) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var supplier = _supplierService.GetById(id);
            var supplierResource = _mapper.Map<Supplier, SupplierDTO>(supplier);
            return Ok(supplierResource);
        }

        [HttpPost]
        public IActionResult CreateSupplier([FromBody] SaveSupplierDTO saveSupplierDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            // Maybe a better way to add client data
            var supplier = _mapper.Map<SaveSupplierDTO, Supplier>(saveSupplierDTO);
            _supplierService.Create(supplier);
            var supplierResource = _mapper.Map<Supplier, SupplierDTO>(supplier);
            return Ok(supplierResource);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateSupplier(Guid id, [FromBody] SaveSupplierDTO saveSupplierDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            // Maybe a better way to add client data
            var updatedSupplier = _mapper.Map<SaveSupplierDTO, Supplier>(saveSupplierDTO);

            var supplier = _supplierService.GetById(id);

            if (supplier == null) {
                return NotFound();
            }

            _supplierService.Update(updatedSupplier);
            var jobResource = _mapper.Map<Supplier, SupplierDTO>(supplier);

            return Ok(jobResource);
        }

        //TODO Check that client doesn't have contact/jobs associated with it
        [HttpDelete]
        public IActionResult DeleteSupplier(Guid id) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var supplier = _supplierService.GetById(id);

            if (supplier == null) {
                return NotFound();
            }

            _supplierService.Delete(supplier.Id);
            return Ok(id);
        }

    }
}
