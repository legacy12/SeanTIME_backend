﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using System;

namespace SeanTIME_bend.Controllers
{
    [Route("api/[controller]")]
    public class QuoteController : Controller {
        private readonly IJobService _jobService;
        private readonly IMapper _mapper;

        public QuoteController(IJobService jobService,
                                IMapper mapper) {
            _jobService = jobService;
            _mapper = mapper;
        }

        [HttpGet]
        public QueryResultDTO<JobDTO> GetQuotes(JobQueryDTO filterResource) {
            var filter = _mapper.Map<JobQueryDTO, JobQuery>(filterResource);
            var queryResult = _jobService.GetQuotes(filter);
            Console.WriteLine("quotes returned: " + queryResult.Items.ToString());
            return _mapper.Map<QueryResult<Job>, QueryResultDTO<JobDTO>>(queryResult);
        }

        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}")]
        public QueryResultDTO<JobDTO> GetQuote(Guid id, JobQueryDTO filterResource) {
            var filter = _mapper.Map<JobQueryDTO, JobQuery>(filterResource);

            var queryResult = _jobService.GetById(id, filter);
            return _mapper.Map<QueryResult<Job>, QueryResultDTO<JobDTO>>(queryResult);
        }

        [HttpPost]
        public IActionResult CreateQuote([FromBody] SaveJobDTO saveQuoteDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var quote = _mapper.Map<SaveJobDTO, Job>(saveQuoteDTO);
            _jobService.Create(quote);
            var quoteResource = _mapper.Map<Job, JobDTO>(quote);
            return Ok(quoteResource);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateQuote(Guid id, [FromBody] SaveQuoteDTO saveJobDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            // Maybe a better way to add client data
            var updatedQuote = _mapper.Map<SaveQuoteDTO, Job>(saveJobDTO);

            var quote = _jobService.GetById(id);

            if (quote == null) {
                return NotFound();
            }

            _jobService.Update(quote, updatedQuote);
            var quoteResource = _mapper.Map<Job, JobDTO>(quote);

            return Ok(quoteResource);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteQuote(Guid id) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var quote = _jobService.GetById(id);

            if (quote == null) {
                return NotFound();
            }

            _jobService.Delete(quote.Id);
            return Ok(id);
        }

        [HttpPut("Upgrade/{id}")]
        public IActionResult UpgradeQuote(Guid id, [FromBody] SaveQuoteDTO saveJobDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            // Maybe a better way to add client data
            var updatedQuote = _mapper.Map<SaveQuoteDTO, Job>(saveJobDTO);

            _jobService.UpgradeQuote(updatedQuote);

            return Ok(updatedQuote.Id);
        }
    }
}