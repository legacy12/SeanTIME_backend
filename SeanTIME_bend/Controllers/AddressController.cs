﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;

namespace SeanTIME_bend.Controllers
{
    [Route("api/[controller]")]
    public class AddressController : Controller
    {
        private readonly IAddressService _addressService;
        private readonly IMapper _mapper;

        public AddressController(IAddressService addressService,
                                IMapper mapper)
        {
            _addressService = addressService;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        public QueryResultDTO<AddressDTO> GetAddresses(Guid Id, AddressQueryDTO filterResource)
        {
            var filter = _mapper.Map<AddressQueryDTO, AddressQuery>(filterResource);
            filter.ClientId = Id;
            var queryResult = _addressService.GetAddresses(filter);

            return _mapper.Map<QueryResult<Address>, QueryResultDTO<AddressDTO>>(queryResult);
        }

        [HttpPost]
        public IActionResult CreateAddress([FromBody] SaveAddressDTO saveAddressDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var address = _mapper.Map<SaveAddressDTO, Address>(saveAddressDTO);
            _addressService.AddAddress(address);

            var addressResource = _mapper.Map<Address, AddressDTO>(address);
            return Ok(addressResource);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteAddress(Guid id)
        {
            _addressService.DeleteAddress(id);

            return Ok();
        }


    }
}