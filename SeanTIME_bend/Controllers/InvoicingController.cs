﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeanTIME_bend.Core;
using AutoMapper;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Core.Models;

namespace SeanTIME_bend.Controllers {
    [Route("api/[controller]")]
    public class InvoicingController : Controller {
        private readonly IJobService _jobService;
        private readonly IContactService _contactService;
        private readonly IMapper _mapper;

        public InvoicingController(IJobService jobService,
                                IMapper mapper,
                                IContactService contactService) {
            _jobService = jobService;
            _contactService = contactService;
            _mapper = mapper;
        }

        [HttpGet]
        public QueryResultDTO<JobDTO> GetJobs(JobQueryDTO filterResource) {
            var filter = _mapper.Map<JobQueryDTO, JobQuery>(filterResource);
            var queryResult = _jobService.GetInvoicing(filter);
            Console.WriteLine("jobs returned: " + queryResult.Items.ToString());
            return _mapper.Map<QueryResult<Job>, QueryResultDTO<JobDTO>>(queryResult);
        }
    }
}