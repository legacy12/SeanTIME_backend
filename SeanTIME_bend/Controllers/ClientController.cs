﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.DTOs;
using SeanTIME_bend.Helpers;
using SeanTIME_bend.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers
{
    [Route("api/[controller]")]
    public class ClientController : Controller {
        private readonly IClientService _clientService;
        private readonly IContactService _contactService;
        private readonly IMapper _mapper;

        public ClientController(IClientService clientService,
                                IMapper mapper,
                                IContactService contactService) {
            _clientService = clientService;
            _contactService = contactService;
            _mapper = mapper;
        }

        [HttpGet]
        public QueryResultDTO<ClientDTO> GetClients(ClientQueryDTO filterResource) {
            var filter = _mapper.Map<ClientQueryDTO, ClientQuery>(filterResource);
            var queryResult = _clientService.GetClients(filter);

            return _mapper.Map<QueryResult<Client>, QueryResultDTO<ClientDTO>>(queryResult);
        }

        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}")]
        public IActionResult GetClient(Guid id) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var client = _clientService.GetById(id);
            var clientResource = _mapper.Map<Client, ClientDTO>(client);

            return Ok(clientResource);
        }

        [HttpPost]
        public IActionResult CreateClient([FromBody] SaveClientDTO saveClientDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var client = _mapper.Map<SaveClientDTO, Client>(saveClientDTO);
            _clientService.Create(client);
            var clientResource = _mapper.Map<Client, ClientDTO>(client);
            return Ok(clientResource);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateClient(Guid id, [FromBody] SaveClientDTO saveClientDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var client = _mapper.Map<SaveClientDTO, Client>(saveClientDTO);

            _clientService.Update(client);
            var clientResource = _mapper.Map<Client, ClientDTO>(client);

            return Ok(clientResource);
        }

        //TODO Check that client doesn't have contact/jobs associated with it
        [HttpDelete("{id}")]
        public IActionResult DeleteClient(Guid id) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var client = _clientService.GetById(id);

            if (client == null) {
                return NotFound();
            }

            _clientService.Delete(client.Id);

            return Ok(id);
        }

        [HttpPut("{id}/OnStop")]
        public IActionResult UpdateOnStop(Guid id, [FromBody] SaveClientDTO saveClientDTO) {

            // TODO Add this back in ASAP
            //if (!ModelState.IsValid) {
            //    return BadRequest(ModelState);
            //}

            var client = _clientService.GetById(id);

            if (client == null) {
                return NotFound();
            }


            _clientService.UpdateOnStop(id, saveClientDTO.OnStop);
            var clientResource = _mapper.Map<Client, ClientDTO>(client);

            return Ok(clientResource);
        }
    }
}
