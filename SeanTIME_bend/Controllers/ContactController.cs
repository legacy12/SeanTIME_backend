﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using SeanTIME_bend.DTOs;
using SeanTIME_bend.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers {
    [Route("api/[controller]")]
    public class ContactController : Controller {
        private readonly IContactService _contactService;
        private readonly ISupplierService _supplierService;
        private readonly IMapper _mapper;

        public ContactController(IMapper mapper,
            IContactService contactService,
            ISupplierService supplierService) {
            _contactService = contactService;
            _supplierService = supplierService;
            _mapper = mapper;

        }

        [HttpGet("Suppliers")]
        public QueryResultDTO<ContactDTO> GetSupplierContacts(ContactQueryDTO filterResource) {
            var filter = _mapper.Map<ContactQueryDTO, ContactQuery>(filterResource);
            var queryResult = _contactService.GetContacts(filter, ContactRole.Supplier);


            return _mapper.Map<QueryResult<Contact>, QueryResultDTO<ContactDTO>>(queryResult);
        }

        [HttpGet("Clients")]
        public QueryResultDTO<ContactDTO> GetClientContacts(ContactQueryDTO filterResource) {
            var filter = _mapper.Map<ContactQueryDTO, ContactQuery>(filterResource);
            var queryResult = _contactService.GetContacts(filter, ContactRole.Client);


            return _mapper.Map<QueryResult<Contact>, QueryResultDTO<ContactDTO>>(queryResult);
        }

        [HttpGet]
        public QueryResultDTO<ContactDTO> GetContacts(ContactQueryDTO filterResource) {
            var filter = _mapper.Map<ContactQueryDTO, ContactQuery>(filterResource);
            var queryResult = _contactService.GetContacts(filter);

            return _mapper.Map<QueryResult<Contact>, QueryResultDTO<ContactDTO>>(queryResult);
        }

        [HttpGet("Unassigned")]
        public QueryResultDTO<ContactDTO> GetUnassignedContacts(ContactQueryDTO filterResource) {
            var filter = _mapper.Map<ContactQueryDTO, ContactQuery>(filterResource);
            var queryResult = _contactService.GetUnassignedContacts(filter);

            return _mapper.Map<QueryResult<Contact>, QueryResultDTO<ContactDTO>>(queryResult);
        }

        [HttpGet("Unassigned/{role}")]
        public QueryResultDTO<ContactDTO> GetUnassignedContacts(ContactQueryDTO filterResource, ContactRole role) {
            var filter = _mapper.Map<ContactQueryDTO, ContactQuery>(filterResource);
            var queryResult = _contactService.GetUnassignedContacts(filter, role);

            return _mapper.Map<QueryResult<Contact>, QueryResultDTO<ContactDTO>>(queryResult);
        }

        [HttpGet("{id}")]
        public IActionResult GetContact(Guid id) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var contact = _contactService.GetById(id);
            var contactResource = _mapper.Map<Contact, ContactDTO>(contact);
            return Ok(contactResource);
        }

        [HttpPost]
        public IActionResult CreateContact([FromBody] SaveContactDTO saveContactDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var contact = _mapper.Map<SaveContactDTO, Contact>(saveContactDTO);
            _contactService.Create(contact);
            var contactResource = _mapper.Map<Contact, ContactDTO>(contact);
            return Ok(contactResource);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateContact(Guid id, [FromBody] SaveContactDTO saveContactDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            var saveContact = _mapper.Map<SaveContactDTO, Contact>(saveContactDTO);
            var contact = _contactService.GetById(id);

            if (contact == null) {
                return NotFound();
            }

            _contactService.Update(id, saveContact);
            var contactResource = _mapper.Map<Contact, ContactDTO>(contact);

            return Ok(contactResource);
        }

        //TODO Check that contact doesn't have jobs associated with it
        [HttpDelete("{id}")]
        public IActionResult DeleteContact(Guid id) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var contact = _contactService.GetById(id);

            if (contact == null) {
                return NotFound();
            }

            Boolean result = _supplierService.GetSupplierFromContact(contact);

            if (result) {
                throw new AppException("", 1);
            }

            _contactService.Delete(contact.Id);
            return Ok(id);
        }
    }
}
