﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers.DTOs
{
    public class JobQueryDTO
    {
        public Guid Id { get; set; }
        public int FriendlyId { get; set; }
        public string Title { get; set; }
        public Contact Contact { get; set; }
        public string Description { get; set; }
        public User User { get; set; }
        public ICollection<Note> Notes { get; set; }
        public Milestones Milestones { get; set; }
        public Guid ClientId { get; set; }
        public ICollection<Item> Items { get; set; }
        public string SortBy { get; set; }
        public bool IsSortAscending { get; set; }
        public int Page { get; set; }
        public byte PageSize { get; set; }
        public JobState JobState { get; set; }
    }
}
