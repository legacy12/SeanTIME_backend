﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.DTOs {
    public class ClientDTO {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Contact Contact { get; set; }
        public User User { get; set; }
        public Guid UserId { get; set; }

        public string Description { get; set; }

        public Boolean OnStop { get; set; }

        public ICollection<Job> Jobs { get; set; }
        public ICollection<Address> Addresses { get; set; }

    }
}
