﻿using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers.DTOs
{
    public class ContactDTO {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int PhoneNumber { get; set; }

        public ContactRole ContactRole { get; set; }

        public ICollection<Job> Jobs { get; set; }

        public string Email { get; set; }
        public Boolean IsPrimary { get; set; }


    }
}
