﻿using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers.DTOs
{
    public class SaveContactDTO
    {

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public Guid Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public ICollection<Job> Jobs { get; set; }

        public ContactRole ContactRole { get; set; }
        public Boolean IsPrimary { get; set; }

    }
}
