﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;

namespace SeanTIME_bend.Controllers.DTOs
{
    public class QuoteDTO {

        public Guid Id { get; set; }
        public int FriendlyId { get; set; }
        public string Title { get; set; }
        public Contact Contact { get; set; }
        public string Description { get; set; }
        public User User { get; set; }
        public Client Client { get; set; }
        public Guid ClientId {get;set; }
        public Guid ContactId { get; set; }
        public Guid UserId { get; set; }
        public Guid SupplierId { get; set; }
        public SeanTIME_bend.Core.Models.Supplier Supplier { get; set; }
        public ICollection<Note> Notes { get; set; }
        public Double PurchasePrice { get; set; }
        public Double SellingPrice { get; set; }
        public int Markup { get; set; }
        public Milestones Milestones { get; set; }
        public ICollection<Item> Items { get; set; }


    }
}
