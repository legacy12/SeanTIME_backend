﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers.DTOs.Supplier
{
    public class SupplierDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int PhoneNumber { get; set; }
        public IEnumerable<Job> Jobs { get; set; }
        public IEnumerable<Issue> Issues { get; set; }
        public IEnumerable<Contact> Contacts { get; set; }
    }
}
