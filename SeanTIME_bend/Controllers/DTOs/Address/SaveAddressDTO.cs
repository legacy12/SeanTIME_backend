﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers.DTOs
{
    public class SaveAddressDTO
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public Client Client { get; set; }
        public string Country { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Postcode { get; set; }
        public string Province { get; set; }
        public string Town { get; set; }
        public string FormattedAddress { get; set; }
        public string Name { get; set; }
        public Boolean IsPrimary { get; set; }
    }
}
