﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers {
    [Route("api/[controller]")]
    public class JobController : Controller {
        private readonly IJobService _jobService;
        private readonly IContactService _contactService;
        private readonly IMapper _mapper;

        public JobController(IJobService jobService,
                                IMapper mapper,
                                IContactService contactService) {
            _jobService = jobService;
            _contactService = contactService;
            _mapper = mapper;
        }

        [HttpGet]
        public QueryResultDTO<JobDTO> GetJobs(JobQueryDTO filterResource) {
            var filter = _mapper.Map<JobQueryDTO, JobQuery>(filterResource);
            var queryResult = _jobService.GetJobs(filter);
            Console.WriteLine("jobs returned: " + queryResult.Items.ToString());
            return _mapper.Map<QueryResult<Job>, QueryResultDTO<JobDTO>>(queryResult);
        }

        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}")]
        public QueryResultDTO<JobDTO> GetJob(Guid id, JobQueryDTO filterResource) {
            var filter = _mapper.Map<JobQueryDTO, JobQuery>(filterResource);

            var queryResult = _jobService.GetById(id, filter);
            return _mapper.Map<QueryResult<Job>, QueryResultDTO<JobDTO>>(queryResult);
        }

        [HttpPost]
        public IActionResult CreateJob([FromBody] SaveJobDTO saveJobDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            // Maybe a better way to add client data
            var job = _mapper.Map<SaveJobDTO, Job>(saveJobDTO);

            _jobService.Create(job);
            var jobResource = _mapper.Map<Job, JobDTO>(job);
            return Ok(jobResource);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateJob(Guid id, [FromBody] SaveJobDTO saveJobDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            // Maybe a better way to add client data
            var updatedJob = _mapper.Map<SaveJobDTO, Job>(saveJobDTO);

            var job = _jobService.GetById(id);

            if (job == null) {
                return NotFound();
            }

            _jobService.Update(job, updatedJob);
            var jobResource = _mapper.Map<Job, JobDTO>(job);

            return Ok(jobResource);
        }

        //TODO Check that client doesn't have contact/jobs associated with it
        [HttpDelete("{id}")]
        public IActionResult DeleteJob(Guid id) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var job = _jobService.GetById(id);

            if (job == null) {
                return NotFound();
            }

            _jobService.Delete(job.Id);
            return Ok(id);
        }

        [HttpPut("Upgrade/{id}")]
        public IActionResult UpgradeJob(Guid id, [FromBody] SaveQuoteDTO saveJobDTO) {

            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            // Maybe a better way to add client data
            var updatedProduction = _mapper.Map<SaveQuoteDTO, Job>(saveJobDTO);

            _jobService.UpgradeProduction(updatedProduction);

            return Ok(updatedProduction.Id);
        }
    }
}
