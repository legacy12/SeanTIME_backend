﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeanTIME_bend.Controllers.DTOs;
using SeanTIME_bend.Core;
using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Controllers
{
    [Route("api/[controller]")]
    public class PreProductionController : Controller {
        private readonly IJobService _jobService;
        private readonly IMapper _mapper;

        public PreProductionController(IJobService jobService,
            IMapper mapper) {
            _jobService = jobService;
            _mapper = mapper;
        }

        [HttpGet]
        public QueryResultDTO<JobDTO> GettPreProductions(JobQueryDTO filterResource)
        {
            var filter = _mapper.Map<JobQueryDTO, JobQuery>(filterResource);
            var queryResult = _jobService.GetQuotes(filter);
            Console.WriteLine("quotes returned: " + queryResult.Items.ToString());
            return _mapper.Map<QueryResult<Job>, QueryResultDTO<JobDTO>>(queryResult);
        }

        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("{id}")]
        public QueryResultDTO<PreProductionDTO> GetPreProduction(Guid id, PreProductionDTO filterResource)
        {
            var filter = _mapper.Map<PreProductionDTO, JobQuery>(filterResource);

            var queryResult = _jobService.GetPreProductionById(id, filter);
            return _mapper.Map<QueryResult<Job>, QueryResultDTO<PreProductionDTO>>(queryResult);
        }

        [HttpPost]
        public IActionResult CreatetPreProduction([FromBody] SavePreProductionDTO savePreProductionDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var preProduction = _mapper.Map<SavePreProductionDTO, Job>(savePreProductionDTO);
            _jobService.Create(preProduction);
            var quoteResource = _mapper.Map<Job, PreProductionDTO>(preProduction);
            return Ok(quoteResource);
        }

        [HttpPut("{id}")]
        public IActionResult UpdatetPreProduction(Guid id, [FromBody] SaveQuoteDTO saveJobDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // Maybe a better way to add client data
            var updatedQuote = _mapper.Map<SaveQuoteDTO, Job>(saveJobDTO);

            var quote = _jobService.GetById(id);

            if (quote == null)
            {
                return NotFound();
            }

            _jobService.Update(quote, updatedQuote);
            var quoteResource = _mapper.Map<Job, JobDTO>(quote);

            return Ok(quoteResource);
        }

        [HttpDelete("{id}")]
        public IActionResult DeletetPreProduction(Guid id)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var quote = _jobService.GetById(id);

            if (quote == null)
            {
                return NotFound();
            }

            _jobService.Delete(quote.Id);
            return Ok(id);
        }

        [HttpPut("Upgrade/{id}")]
        public IActionResult UpgradetPreProduction(Guid id, [FromBody] SaveQuoteDTO saveJobDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Maybe a better way to add client data
            var updatedQuote = _mapper.Map<SaveQuoteDTO, Job>(saveJobDTO);

            _jobService.UpgradeQuote(updatedQuote);

            return Ok(updatedQuote.Id);
        }
    }
}
