﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core
{
    public interface IJobService
    {
        QueryResult<Job> GetCompleted(JobQuery queryObj);

        QueryResult<Job> GetInvoicing(JobQuery queryObj);

        QueryResult<Job> GetQuotes(JobQuery queryObj);

        QueryResult<Job> GetJobs(JobQuery queryObj);

        QueryResult<Job> GetById(Guid id, JobQuery queryObj);

        QueryResult<Job> GetPreProductionById(Guid id, JobQuery queryObj);

        Job GetById(Guid id);

        QueryResult<Job> GetByClient(JobQuery queryObj);

        Job Create(Job job);

        void Update(Job job, Job jobParam);

        void Delete(Guid id);

        void UpgradeQuote(Job jobParam);

        void UpgradePreProduction(Job jobParam);

        void UpgradeProduction(Job jobParam);

        void UpgradeInvoicing(Job jobParam);
    }
}
