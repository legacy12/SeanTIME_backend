﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core
{
    public interface IQuoteService
    {
        QueryResult<Quote> GetQuotes(QuoteQuery queryObj);

        Quote GetById(Guid id);

        QueryResult<Quote> GetById(Guid id, QuoteQuery queryObj);

        Quote Create(Quote quote);

        void Update(Quote quote, Quote quoteParam);

        void Delete(Guid id);

        void ChangeToJob(Guid id);
    }
}
