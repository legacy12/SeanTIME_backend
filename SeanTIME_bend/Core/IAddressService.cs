﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core
{
    public interface IAddressService
    {
        QueryResult<Address> GetAddresses(AddressQuery filter);

        void AddAddress(Address address);

        void EditAddress(Address address);

        void DeleteAddress(Guid id);
    }
}
