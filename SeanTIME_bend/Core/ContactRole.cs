﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core
{
    public enum ContactRole
    {
        Lead,
        Client,
        Job,
        Supplier
    }
}
