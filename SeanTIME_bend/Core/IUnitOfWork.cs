﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core { 
    public interface IUnitOfWork {
    Task CompleteAsync();
}
}
