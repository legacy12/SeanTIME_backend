﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core
{
    public interface IContactService
    {
        QueryResult<Contact> GetContacts(ContactQuery queryObj, ContactRole role);

        QueryResult<Contact> GetContacts(ContactQuery queryObj);

        QueryResult<Contact> GetUnassignedContacts(ContactQuery queryObj);

        QueryResult<Contact> GetUnassignedContacts(ContactQuery queryObj, ContactRole role);

        Contact GetById(Guid id);

        Contact Create(Contact contact);

        void Update (Guid id, Contact contact);

        void Delete(Guid id);
    }
}
