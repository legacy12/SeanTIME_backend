﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core
{
    public interface IClientService
    {
        QueryResult<Client> GetClients(ClientQuery filter);

        Client GetById(Guid id);

        Client Create(Client client);

        void Update(Client client);

        void UpdateOnStop(Guid id, Boolean OnStop);

        void Delete(Guid id);
    }
}
