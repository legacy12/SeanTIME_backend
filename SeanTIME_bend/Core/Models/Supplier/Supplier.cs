﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models {

    [Table("Suppliers")]
    public class Supplier {

        public Guid Id { get; set; }

        public string Name { get; set; }

        public int PhoneNumber { get; set; }

        public ICollection<Item> Items { get; set; }

        public ICollection<Issue> Issues { get; set; }

        public ICollection<Contact> Contacts { get; set; }

        public Supplier() {
            Items = new Collection<Item>();
            Issues = new Collection<Issue>();
            Contacts = new Collection<Contact>();
        }
    }
}
