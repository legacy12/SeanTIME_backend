﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    [Table("Milestones")]
    public class Milestones
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }   // Quote
        public DateTime Due { get; set; } 
        public DateTime Started { get; set; }   //Job
        public DateTime Updated { get; set; }   
        public DateTime Invoiced { get; set; }
        public DateTime Completed { get; set; } //Paid

    }
}
