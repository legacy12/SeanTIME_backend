﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    [Table("Jobs")]
    public class Job {

        public Guid Id { get; set; }
        public int FriendlyId { get; set; }
        public Guid ContactId { get; set; }
        public Guid ClientId { get; set; }
        public Guid UserId { get; set; }
        public Client Client { get; set; }
        public Contact Contact { get; set; }
        public User User { get; set; }
        public ICollection<Note> Notes { get; set; }
        public Milestones Milestones { get; set; }
        public ICollection<Item> Items { get; set; }
        public ICollection<PreItem> PreItems { get; set; }
        public JobState JobState { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DeliveryAddress { get; set; }

        public Job()
        {
            Items = new Collection<Item>();
            PreItems = new Collection<PreItem>();
            Notes = new Collection<Note>();
        }

    }
}
