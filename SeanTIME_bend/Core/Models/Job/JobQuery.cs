﻿using SeanTIME_bend.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    public class JobQuery : IQueryObject
    {
        public Guid Id { get; set; }
        public ICollection<Note> Notes { get; set; }
        public JobState JobState { get; set; }
        public Guid ClientId { get; set; }
        public ICollection<PreItem> PreItems { get; set; }
        public User User { get; set; }
        public Contact Contact { get; set; }
        public int FriendlyId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DeliveryAddress { get; set; }
        public string SortBy { get; set; }
        public bool IsSortAscending { get; set; }
        public int Page { get; set; }
        public byte PageSize { get; set; }
    }
}
