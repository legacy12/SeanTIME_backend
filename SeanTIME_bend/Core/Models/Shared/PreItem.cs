﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    [Table("PreItems")]
    public class PreItem
    {
        public Guid Id { get; set; }
        public Guid JobId { get; set; }
        public Job Job { get; set; }
        public string Product { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public ICollection<SupplierContactItem> SupplierContactItems { get; set; }
    }
}
