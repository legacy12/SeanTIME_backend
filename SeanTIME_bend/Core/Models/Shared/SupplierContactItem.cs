﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    [Table("SupplierContactItems")]
    public class SupplierContactItem
    {
        public Guid Id { get; set; }

        public Guid ContactId { get; set; }

        public Guid SupplierId { get; set; }

        public Contact Contact { get; set; }

        public Supplier Supplier { get; set; }
    }
}
