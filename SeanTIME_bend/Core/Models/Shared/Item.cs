﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    [Table("Items")]
    public class Item
    {
        public Guid Id { get; set; }
        public Guid? JobId { get; set; }
        public string Product { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public Supplier Supplier { get; set; }
        public Guid SupplierId { get; set; }
    }
}
