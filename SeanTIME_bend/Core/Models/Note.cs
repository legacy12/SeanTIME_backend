﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    [Table("Notes")]
    public class Note
    {
        public Guid Id { get; set; }

        public Guid JobId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [StringLength(255)]
        public string Message { get; set; }

        [Required]
        [StringLength(255)]
        public string StaffName { get; set; }

    }
}
