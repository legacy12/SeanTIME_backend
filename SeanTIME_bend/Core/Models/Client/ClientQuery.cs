﻿using SeanTIME_bend.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    public class ClientQuery : IQueryObject
    {
        public string Name { get; set; }
        public Contact Contact { get; set; }
        public User User { get; set; }
        public Guid UserId { get; set; }
        public string Description { get; set; }
        public IEnumerable<Job> Jobs { get; set; }
        public ICollection<Address> Addresses { get; set; }
        public Boolean OnStop { get; set; }
        public string SortBy { get; set; }
        public bool IsSortAscending { get; set; }
        public int Page { get; set; }
        public byte PageSize { get; set; }
    }
}
