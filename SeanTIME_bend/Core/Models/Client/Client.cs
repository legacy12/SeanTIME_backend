﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    [Table("Clients")]
    public class Client
    {
        public Guid Id { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public Contact Contact { get; set; }

        public User User { get; set; }

        public Guid UserId { get; set; }

        public Guid ContactId { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public ICollection<Job> Jobs { get; set; }
        public ICollection<Address> Addresses { get; set; }

        public Boolean OnStop { get; set; }

        public Client() {
            Jobs = new Collection<Job>();
            Addresses = new Collection<Address>();
        }
    }
}
