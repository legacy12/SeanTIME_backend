﻿using SeanTIME_bend.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeanTIME_bend.Core;
namespace SeanTIME_bend.Core.Models
{
    public class ContactQuery : IQueryObject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public IEnumerable<Job> Jobs { get; set; }
        public string SortBy { get; set; }
        public bool IsSortAscending { get; set; }
        public int Page { get; set; }
        public byte PageSize { get; set; }
        public ContactRole ContactRole { get; set; }
        public Boolean IsPrimary { get; set; }

    }
}
