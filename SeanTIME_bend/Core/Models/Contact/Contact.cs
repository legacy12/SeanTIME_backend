﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core.Models
{
    [Table("Contacts")]
    public class Contact
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public int PhoneNumber { get; set; }

        public Supplier Supplier { get; set; }

        public Client Client { get; set; }

        public ContactRole ContactRole { get; set; }

        public ICollection<Job> Jobs { get; set; }
        public Boolean IsPrimary { get; set; }


        public Contact() {
            Jobs = new Collection<Job>();
        }
    }
}
