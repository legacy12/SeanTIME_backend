﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace SeanTIME_bend.Core.Models
{
    [Table("Issues")]
    public class Issue
    {
        public Guid Id { get; set; }

        public DateTime Created { get; set; }

        public string Message { get; set; }
    }
}
