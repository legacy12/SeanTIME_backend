﻿using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Core
{
    public interface ISupplierService
    {
        QueryResult<Supplier> GetSuppliers(SupplierQuery queryObj);

        Supplier GetById(Guid id);

        Supplier Create(Supplier supplier);

        Boolean GetSupplierFromContact(Contact contact);

        void Update(Supplier supplier);

        void Delete(Guid id);
    }
}
