﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Helpers 
    {
    public class RequestResult {
        public RequestState State { get; set; }
        public ErrorCode ErrorCode { get; set; }
        public string Msg { get; set; }
        public Object Data { get; set; }
    }

    public enum RequestState {
        Failed = -1,
        NotAuth = 0,
        Success = 1
    }

    public enum ErrorCode {
        ContactInUse = 1
    }
}