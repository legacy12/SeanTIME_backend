﻿using Microsoft.EntityFrameworkCore;
using SeanTIME_bend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

//https://docs.microsoft.com/en-us/ef/core/modeling/relationships

namespace SeanTIME_bend.Persistance {
    public class SeanTIMEDbContext : DbContext {
        public DbSet<User> User { get; set; }

        public DbSet<Client> Client { get; set; }

        public DbSet<Supplier> Supplier { get; set; }

        public DbSet<Job> Job { get; set; }

        public DbSet<Contact> Contact { get; set; }

        public DbSet<Note> Note { get; set; }

        public DbSet<Item> Item { get; set; }

        public DbSet<Milestones> Milestones { get; set; }

        public DbSet<Issue> Issues { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<SupplierContactItem> SupplierContactItems { get; set; }

        public SeanTIMEDbContext(DbContextOptions<SeanTIMEDbContext> options)
            : base(options) {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {

            modelBuilder.Entity<Job>()
                .Property(e => e.FriendlyId)
                .ValueGeneratedOnAdd()
                .Metadata.AfterSaveBehavior = Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Throw;

            // Client tree
            modelBuilder.Entity<Client>()
                .HasOne(c => c.Contact);
            modelBuilder.Entity<Client>()
                .HasMany(c => c.Jobs);

            //User tree
            modelBuilder.Entity<User>()
                .HasMany(u => u.Jobs);

            //Contact tree
            modelBuilder.Entity<Contact>()
                .HasMany(u => u.Jobs);

            //Job tree
            modelBuilder.Entity<Job>()
                .HasMany(j => j.Notes);
            modelBuilder.Entity<Job>()
                .HasMany(j => j.Items);
            modelBuilder.Entity<Job>()
                .HasOne(j => j.Milestones);
        }
    }
}
