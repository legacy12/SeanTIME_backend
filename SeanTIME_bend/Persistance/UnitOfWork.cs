﻿using SeanTIME_bend.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeanTIME_bend.Persistance { 
    public class UnitOfWork : IUnitOfWork {

    private readonly SeanTIMEDbContext context;

    public UnitOfWork(SeanTIMEDbContext context) {
        this.context = context;
    }
    public async Task CompleteAsync() {
        await context.SaveChangesAsync();
    }
}
}
